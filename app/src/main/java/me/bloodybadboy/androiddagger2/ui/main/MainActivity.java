package me.bloodybadboy.androiddagger2.ui.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;
import dagger.android.AndroidInjection;
import javax.inject.Inject;
import me.bloodybadboy.androiddagger2.R;
import me.bloodybadboy.androiddagger2.api.JokeService;

public class MainActivity extends AppCompatActivity implements MainContract.View {

  @Inject
  public JokeService jokeService;

  @Inject
  public MainContract.Presenter presenter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    AndroidInjection.inject(this);
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    findViewById(R.id.btn_main_tell_joke).setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        presenter.onClickTellMeAJoke();
      }
    });
  }

  @Override public void showJoke() {
    String message = String.format("%s", jokeService.getAJoke());
    Toast.makeText(this, message, Toast.LENGTH_LONG).show();
  }
}
