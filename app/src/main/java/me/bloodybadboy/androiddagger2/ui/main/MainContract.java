package me.bloodybadboy.androiddagger2.ui.main;

public interface MainContract {

  interface Presenter {
    void onClickTellMeAJoke();
  }

  interface View {
    void showJoke();
  }
}
