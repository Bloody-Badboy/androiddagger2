package me.bloodybadboy.androiddagger2.ui.main;

public class MainPresenter implements MainContract.Presenter {
  public MainContract.View view;

  public MainPresenter(MainContract.View view) {
    this.view = view;
  }

  @Override public void onClickTellMeAJoke() {
    view.showJoke();
  }
}
