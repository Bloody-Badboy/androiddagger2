package me.bloodybadboy.androiddagger2.ui.main.di;

import dagger.Module;
import dagger.Provides;
import me.bloodybadboy.androiddagger2.ui.main.MainActivity;
import me.bloodybadboy.androiddagger2.ui.main.MainContract;
import me.bloodybadboy.androiddagger2.ui.main.MainPresenter;

@Module
public class MainActivityModule {

  @Provides
  public MainContract.View provideMainContractView(MainActivity mainActivity) {
    return mainActivity;
  }

  @Provides
  public MainContract.Presenter provideMainContractPresenter(MainContract.View view) {
    return new MainPresenter(view);
  }
}
