package me.bloodybadboy.androiddagger2.ui.main.di;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import me.bloodybadboy.androiddagger2.ui.main.MainActivity;

// sub-component is created manually, have to write boilerplate code :P
@Subcomponent(modules = MainActivityModule.class)
public interface MainActivityComponent extends AndroidInjector<MainActivity> {
  @Subcomponent.Builder
  abstract class Builder extends AndroidInjector.Builder<MainActivity> {
  }
}