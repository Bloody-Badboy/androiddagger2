package me.bloodybadboy.androiddagger2.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class JokeService {
  private static List<String> jokes = new ArrayList<>();
  private static final Random random = new Random();

  static {
    jokes.add(
        "“Debugging” is like being the detective in a crime drama where you are also the murderer.");
    jokes.add("!false\n(It’s funny because it’s true.)");
    jokes.add("If you listen to a UNIX shell, can you hear the C?");
    jokes.add(
        "A programmer puts two glasses on his bedside table before going to sleep. A full one, in case he gets thirsty, and an empty one, in case he doesn’t.");
    jokes.add("[“hip”,”hip”] (hip hip array!)");
    jokes.add(
        "An optimist says “The glass is half full.”\nA pessimist says “The glass is half empty.”\nA programmer says “The glass is twice as large as necessary.”");
    jokes.add("why do cows wear bells? because their horns don't work!");
    jokes.add("Knowing how to pick locks has really opened a lot of doors for me.");
  }

  public String getAJoke() {
    return jokes.get(random.nextInt(jokes.size()));
  }
}
