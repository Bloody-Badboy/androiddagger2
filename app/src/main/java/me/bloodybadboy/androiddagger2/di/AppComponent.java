package me.bloodybadboy.androiddagger2.di;

import android.app.Application;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;
import me.bloodybadboy.androiddagger2.App;

@Component(modules = { AndroidInjectionModule.class, AppModule.class ,ActivityBindingModule.class })
public interface AppComponent extends AndroidInjector<App> {
  @Component.Builder interface Builder {

    @BindsInstance
    AppComponent.Builder application(Application application);

    AppComponent build();
  }

  void inject(App app);
}
