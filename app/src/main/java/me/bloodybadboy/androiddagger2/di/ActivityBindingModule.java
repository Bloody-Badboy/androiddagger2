package me.bloodybadboy.androiddagger2.di;

import android.app.Activity;
import dagger.Binds;
import dagger.Module;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.android.ContributesAndroidInjector;
import dagger.multibindings.IntoMap;
import me.bloodybadboy.androiddagger2.ui.main.MainActivity;
import me.bloodybadboy.androiddagger2.ui.main.di.MainActivityComponent;
import me.bloodybadboy.androiddagger2.ui.main.di.MainActivityModule;

// if sub-component is created manually
// @Module(subcomponents = MainActivityComponent.class)

@Module
abstract class ActivityBindingModule {

  @ContributesAndroidInjector(modules = { MainActivityModule.class })
  abstract MainActivity contributeMainActivity();

  // extra boilerplate code if sub-component is created manually
  /*@Binds
  @IntoMap
  @ActivityKey(MainActivity.class)
  abstract AndroidInjector.Factory<? extends Activity> bindMainActivity(
      MainActivityComponent.Builder builder);*/
}
