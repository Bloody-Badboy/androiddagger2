package me.bloodybadboy.androiddagger2.di;

import dagger.Module;
import dagger.Provides;
import me.bloodybadboy.androiddagger2.api.JokeService;

@Module
public class AppModule {

  @Provides
  public static JokeService provideJokeService() {
    return new JokeService();
  }
}
