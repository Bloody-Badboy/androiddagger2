package me.bloodybadboy.androiddagger2;

import android.app.Activity;
import android.app.Application;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import javax.inject.Inject;
import me.bloodybadboy.androiddagger2.di.DaggerAppComponent;

public class App extends Application implements HasActivityInjector {
  @Inject
  public DispatchingAndroidInjector<Activity> androidInjector;

  @Override public void onCreate() {
    super.onCreate();
    DaggerAppComponent.builder()
        .application(this)
        .build()
        .inject(this);
  }

  @Override public AndroidInjector<Activity> activityInjector() {
    return androidInjector;
  }
}
